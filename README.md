# Mas 2021

Présentation aux journées MAS 2020.

markdown/pandoc/katex/reveal.js

Les images sont dans `img`

Pour un usage full browser (nécessite une connection pour le rendering)
```
pandoc -c style.css -t revealjs -s --katex=https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/ myslides.md -o slides.html -V theme=simple -V revealjs-url=https://unpkg.com/reveal.js@3.9.2/
```
